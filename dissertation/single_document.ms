.RP no
.P1
.ds DY
.so ./references/apa-ms.tmac
.R1
database ./references/references.ref
sort A+
accumulate
move-punctuation
label "(@|Q) ', ''\*[MIDINFO]' D '\*[ENDINFO]'"
short-label "'\*[MIDINFO]' D '\*[ENDINFO]'"
bracket-label " (" ")" "; "
join-authors " & " ", " " & "
no-label-in-reference
.R2

.TL
.hy 0
FROM NORMALIZATION TO CELEBRATION: Using social media and open source data
to track narco terrorism
.hy 1

.sp
.sp

.AU
Aaron L. Jones

.AI
Capitol Technology University

.AU
Dr. Michael Clayborn

.(C
2023-02-12
.)C

.bp

.LP

.NH
.XN "Acknowledgements"

.PP
Reflecting deeply on my time spent at Capitol Technology University, it is
plain to any one who can see that none of my accomplishments would be possible
without the innumerable contributions made by my incredible mentors, supportive
friends, and patient family members.
.
It has not been lost on me that I could not have achieved my goal of receiving
my PhD without the support of the University Of Advancing Technology.
.
I am very luck that my employer chose to support me in my education journey and
provided me both the means as well as the encouragement to pursue my degree
plan.
.

.bp

.LP

.NH
.XN "Abstract"

.PP
Promoted and performed globally, narcocorridos are evocative and emotionally
driven ballads that soulfully describe and detail the exploits of Mexican
trafickers and assassins involved in narcotics.
.
The first popular narcocorridos landed in the 1970s but their explosive rise
into the mainstream started in the 1990s. Since the arrival of the
narcocorrido, it has been possible for anyone to hear about the ups and downs
about the narcotics trafficking business based on the songs that have provided
a detailed and sometimes deeply intimate view into the world of the drug
dealer, traficker, and narcoterrorist.
.
How does this music benefit researchers and investigators interested in combatting the
threat actors who are being praised and complimented by song?
.
By analyzing the comments and the music itself, investigators can gain a more
thorough and clear picture into the criminal underworld by tapping the social
queues, support, and knowledge base that the public has about the criminals and
their exploits. 

.bp
.LP

.TC

.bp
.LP

.NH
.XN "Introduction"

.PP
Dating back to 1930, the earliest examples of corridos focused on narcotics
were sung by Juan Ramirez-Pimienta.
.
Typically the narcocorrido will speak of
real events, dates, and places and are positive in their opinion of the
activities of the drug trafickers are their actions.
.
The production of corridos focused on narcotics has become a thorn in officials
sides and government officials in Mexico have proposed drastic actions such as
banning narcocorridos.
.
The state of Chihuahua banned narcocorridos after passing a bill
introduced on April 12th by Congressman Ricardo Alan Boone.
.[
alb-jrnl-banned-in-chihuahua
.]
.
Premised on a belief that the music that glorifies and supports narco
terrorists benefits from being fact based, my research aims to answer the
following questions:
.
.sp
.nr step 1 1
.IP \n[step] 7
What is a narcocorrido?
.IP \n+[step]
What information can be obtained by the songs and their supporters.
.IP \n+[step]
Is this information useful to investigators?
.IP \n+[step]
Are there ethical issues with obtaining our data?
.IP \n+[step]
Are there technical issues with obtaining our data?
.IP \n+[step]
What can analysis of the data provide to an investigator?
.IP \n+[step]
What can we achieve by analyzing this data?

.bp

.LP

.NH
.XN "Literature Review"

.PP
.ls 2
The narcocorrido is a music based approach to old tales of romanticized heroism
and behavior.
.
Songs provide an often accurate view into the criminal syndicates that control
the drug trade.
.
The music appeals to a wide array of audience members and the lyrics often
celebrate drug lords, violence, or the punishment meted out by law enforcement
and how it influences their lives.
.
Given the pivotal role that culture and public sentiment plays in the outcomes
of law enforcement, it is vital that we establish whether or not the music
being produced is reducing the likelihood that the public develops a negative
sentiment for these criminal enterprises.

.PP
.ls 2
Public sentiment, the collective attitudes and beliefs of the general
population towards law enforcement, is shaped by a range of factors that
include media coverage, high-profile cases, and personal experiences with law
enforcement.
.
When there is an alignment between culture and public sentiment in favor of law
enforcement, this can lead to positive outcomes.
.
Agencies that emphasize community policing and positive public relations will
foster productive relationships that instill strong bonds between the agency
and the public.
.
However, when public sentiment is shaped by negative processes, like the
narcocorrido, we find that the public will often times have a negative view of
law enforcement and this creates tension between the agency and their communit.
.
This tension will breed mistrust and failures in cooperation.
.
Understanding and addressing the factors that the narco corridos bring to
communities can help law enforcement improve the overall outcome of law
enforcement.

.PP
.ls 2
A study has shown that violence has become a controversial but vital component
to the narcocorrido and considered a particular focus to song writers and
performers.
.[
narco-transnational-identity
.]
These references to violence include murder, torture, and positive opinions on
the violent deployment of weapons.  
.
Affirmations of Mexican Identity are strongly tied to statehood.
.
Individuals who identify as Mexican will often express their identity by
indicating the specific state they express lineage from.
.
This includes Durango, Sinaloa, or Michoacan to name a few. 
.
This identity of state is also considered an indicator of temperament or trait.
.
Individuals from certain areas or zones can be described as calm, determined,
or brave when descriptors are provided.
.[
narco-transnational-identity
.]

.PP
.ls 2
Sinaloans are often associated with bravery, daring, and toughness.
.
The State of Sinaloa, located in northwestern Mexico, reputedly produces very
brave men due to their connection to drug trafficking and organize crime.
.
Individuals from Sinaloa are portrayed as fearless and willing to take risks.
.
This identification and cultural identity is depicted in similar ways depending
on the individuals location.

.PP
.ls 2
Durango persons are considered bold and fearless.
.
The state of Durango is located in North-Central Mexico, and people from this
region are daring and tough.
.
Regardless of where a person comes from, they will often be portrayed as
overly simplified charicatures of masculinity in order to attract others to the
life style.
.
This type of stereotyping is very harmful.

.PP
.ls 2
Masculinity and what constitutes a man is a major component of the narcocorrido
and narco culture.
.[
corridos-drugs-violence-analysis
.]
The perception of the peer as well as strangers is vital to the narco and their
sense of self worth.
.
This need to determine a pecking order as well as to affirm the gender based
role of the man means that the majority of the music is going to celebrate
these traits.
.
Respect and masculinity are paramount in these relationships and so it is very
important to the narco that others are able to celebrate not only what they are
doing, but who they are as a person and what their actions represent.
.[
corridos-drugs-violence-analysis
.]
The number of social and political conflicts and the considerable amount of
violence that has been weaved in between those events is indicative of how
brutal Mexican history has been and how it is often a point of cultural pride
among the Mexican people.
.
It is only natural that the narcocorrido would be an extension of this
lifestyle.

.PP
.ls 2
The lifestyle of the narco is completely separate from that of the average
Mexican.
.
El Narco is a way of life that is completely different from anything that
others may practice.
.[
elnarco-role-mexican-cartel
.]
The narco will dress in their own way, listen to their own style of music, and
will even worship their own religious sect.
.
The trifecta of clothing, art, and religion gives a certain mystique to the
narco that would otherwise not exist without this entrenched method for
elevating the drug dealer above his peers.
.
This elevation of the narco and creation of a higher class of person may be a
strong reason why the Mexican people have put up with the level of violence
they have seen for so long due to the actions of these persons.
.[
elnarco-role-mexican-cartel
.]
The narco is a priest, a trend setter, and a warrior.
.
He is the embodiment of masculinity.
.
The narco is something more than a mere mortal and approaches something closer
to a legendary figure than a simple farmer or someone who does not rebel
against the system.

.PP
.ls 2
If the narco is such a considerable figure in the history, culture, and the
very civilization of the Mexican people, then it is important to understand the
full history of this well celebrated figure.
.
Treachery, excitement, and violence are the ingrediants that form the drug
trade.
.[
heroes-or-villains-corrido-tradition
.]
The music is a reflection of the people who participate in this lifestyle.
.
Larger than life figures who have become the center of their own epics.
.
The narcocorrido helps to reconcile the contradictory element of the bandit
becoming the hero.
.[
heroes-or-villains-corrido-tradition
.]
The narco is the perfect anti-hero.

.PP
.ls 2
The narco may be called the noble robber, and find himself compared to European
equivalencies such as a Robbin Hood, but the truth is that violence is much
more central to this figure.
.[
los-gallos-valientes
.]
The narco does not appeal because he rights wrongs or functions as a hero
figure, but instead because he demonstrates that the poor and weak can have the
ability and the capability to be a terrible and destructive force.
.[
los-gallos-valientes
.]
American rap music and narcocorridos share many similarities in terms of
content and theme.
.nr step 1 1
These similarities include:
.IP \n[step] 4
The celebration of a criminal lifestyle. Both genres of music will feature
lyrics that glorify the criminal activities and the nuances of the lifestyle
that is associated with those events. In rap, this will include drug usage or
sales, violence, or gang activity. The narcocorrido will often involve drug
smuggling, money related crimes, and violence.
.IP \n+[step]
Both genres of music are methods to tell a story. The experiences and lifestyle
that is glorified in both genres will often support a marginalized community.
Both will include heroes who grew up in poverty, involve themselves in
violence, and deal with law enforcement.
.IP \n+[step]
A criticism of society is also very important in both genres. Inequality,
racism, povery, and police brutality will often take center stage in both
genres of music.
.IP \n+[step]
Regional identity is also a valuable aspect of both genres of music. Rap music
has roots in African American culture while narcocorridos are associated with
Mexican cultural identity.

.PP
.ls 2
In the culture of the narco, honor and power intersect with masculinity in a
complex and sometimes misunderstood way.
.
Masculinity in narco culture is defined as the ability to exert power and
control over other through violence.
.
Narcos will portray themselves as hypermasculine figures who deploy violence as
a method to demonstrate their dominance over others and to protect the
integrity of their sense of honor.
.[
mi-rey-el-narco
.]
This use of violence and the threats of violence are common demonstration tools
for showing strength and dominance over others.

.PP
.ls 2
The narco is expected to be loyal to both family as well as to associate.
.
The need to protect their honor at all costs is often glorified in song and
indicates that they are the embodiment of masculine.
.
The willingness to deploy violence to defend their reputation and to avenge
even the most tenuous perception of slight against them is another indicator
that they are masculine.
.
The narco will also sing about wealth, clothing, and expensive cars to
demonstrate that they are masculine.
.[
mi-rey-el-narco
.]
This lavish lifestyle is an indicator that they are dominant and that they can
lay claim to an alpha role in society.
.
The intersection of these traits is glorified heavily in narcoculture and
within the narcocorrido.

.PP
.ls 2
It is simple to argue to that the narcocorrido glorifies criminal behavior and
that there is a negative impact on society because of the content of the music.
.
A moral panic has flourished due to the actions of narcos and their fans on
social media who are demonstrating their life style via song, image, and video.
.[
narco-moral-panics
.]
A highly polarized community of individuals who both support as well as
function as detractors of the narco lifestlye have flourished on social media.
.
It is easy to conderm Mexican culture and tradition as a source of moral decay
and it is not common for individuals to claim that listening to the
narcocorrido is tantamount to criminal intent.
.[
narco-moral-panics
.]
Cartel TikTok, a catch all phrase for the culture of the narco on the social
media platform TikTok, exists as a perfect example of how the display of the
narco lifestyle is interpreted as harmful to the public.
.[
narco-moral-panics
.]
Drug traffickers can use social media such as TikTok to recruit workers by
glorfying their lifestyle with a combination of short videos, narcocorridos,
and a hefty dose of celebration.

.PP
.ls 2
Conservative groups in Mexico are now calling for a total boycott of companies
that sponsor narcocorrido concerts, and some politicians have gotten behind
this by proposing legislation to ban the music from any public function.
.
Music is a reflection of our values.
.
The values of a person are often but not always a reflection of the values held
by society at large.
.
It would be a gross oversimplification to claim that the production of music is
an expression of our experiences.
.
We express spirituality, discipline, tradition, rebellion, and independence
with the lyrics and genres of music we listen to.
.
We must question then, does the message conveyed by our music influence us and
shape our values over time?
.
If we repeatedly hear music that expresses certain values, would it then make a
lasting impact on our worldview?
.
Do you need to be a criminal to enjoy music about criminality?
.
Can listening to music about criminal activity make you a criminal?

.PP
.ls 2
The ancient Greek "catharsis hypothesis" claims that the expression of
aggressive emotion will later decrease aggressive behavior.
.
Catharsis is highly debated in psychology circles but an understanding of the
concept may help to cast light on why there is not universal condemnation of
the narcocorrido and narco culture.
.
Catharsis is viewed as a method to purify the emotions and to settle the
spiritual tumult that some people may feel.
.
Repressed emotions may build up and cause physical health problems like
depression or hypertension.
.
By expressing emotions through tools like music, individuals will achieve
catharsis and release the emotional tension they are experiencing.
.
Does the narcocorrido provide catharsis and provide an outlet for the venting
of negative emotions?

.PP
.ls 2
The role of music is varied but also functions to promote a particular
viewpoint and could be considered a propaganda mechanism.
.[
narco-and-nostalgia
.]
The drug war has had numerous methods for pushing policies from both sides of
the drug war.
.
Some of the slogans that have been used by the American government have
included, "Just Say No", "Drug-Free America", "War on Drugs", "Gateway Drug",
and "Drug dealers are evil".
.
More modern incarnations of these slogans also include the hyper violent "Kill
your local drug dealer", that is now popular with individuals involved in
combatting the fentanyl crisis.
.
Propaganda on both sides of the aisle will often oversimplify the world view
that someone is right and someone else is wrong.
.[
narco-and-nostalgia
.]

.PP
.ls 2
Because music can evoke strong emotions and the idea of catharsis is still not
a scientific fact, we must assume that music could be used to manipulate a
person of both their thoughts as well as their opinions.
.
The narcocorrido creates a sense of unity and national pride.
.
The narcocorrido functions as a beacon for the idealogy of the drug dealer.
.
The narcocorrido promotes obedience to and glorification of the narco.
.
The narcocorrido builds support for the conflicts and wars fought by the drug
dealers.
.
The narcocorrido can also be used to distract people from the hardships of
their lifestyle choice.
.
Music is an extremely powerful tool in the propaganda realm because of what it
can accomplish when used to influence persons.

.PP
.ls 2
The very act of banning or censoring music becomes a rally point for those who
wish to use music as a method of defiance.
.
Certain types of music are banned by governments in a variety of countries
because of the belief that the music itself is subversive.
.
The response of the musician and the fan is to challenge that authority by the
use of the very music that they are forbidden to enjoy.
.
The decision to ban or remove the narcocorrido from common play would be
devestating if it only encouraged more people to begin listening.
.
Narco music is defiance personified and those who enjoy the music are
expressing their disdain for what they perceive as the mundane.
.[
narco-music-defiance-violence
.]

.PP
.ls 2
Music is not inherently violent but the use of themes and lyrics that glorify
music can be impactful on society in totality.
.
Music that glorifies violent themes and criminal behavior can be argued to be
artistic expression but may also contribute to the desensitization towards and
acceptance of violence on a whole.
.[
narco-music-defiance-violence
.]
It is important however to remember that the narcocorrido could not exist
without the violence in Mexico.
.

.PP
.ls 2
Symbolism plays a crucial role in gang culture and provides members of these
outsider groups with the ability to communicate with each other.
.[
narco-and-violence-in-corridos
.]
Hand signs, tattoos, logos, and colors are some of the methods by which
individuals can communicate affiliation and loyalty to a specific group.
.
The music is part of that symbolism and by listening to certain songs, these
affiliates communicate their perceived capabilities to be violent, their
loyalty, and their commitment to the lifestyle.
.
The use of this symbolism can contribute to a cycle of violence and conflict.
.
We see this in the number of individuals involved in the narcocorrido culture
who have lost their lives to violence.

.PP
.ls 2
A major threat to the Mexican government is organized crime and drug violence.
.[
propaganda-in-mexico-drug-war
.]
Critics will argue that any focus on the propaganda being used in the drug war
is ignoring the causes of the conflict.
.
Drug problems, poverty, and social issues are prevelant in Mexico but that does
not minimize the impact that basing cultural pride on these aspects of life
does to your society.
.
There are arguments that encouraging and romanticizing drug usage can make it
easier for abusers to seek help and treatment by reducing the stigma
surrounding usage.

.PP
.ls 2
The drug trade, the violence, and the lifestyle of the narco is a culture and
has a tremendous impact on the perceptions of those who are exposed to it.
.
By understanding semiotics, or the study of the signs and symbols that are
interpreted by person, we can better analyze how narco culture is communicated
to others.
.[
sociosemiotics-of-mexican-narcoculture
.]
Narco culture is a mass culture built from a variety of subcultures which
culminates in the narco culture we see today.
.
Fashion, language, and iconography create a sense of identity and belonging to
the culture and the songs of the narcocorrido are the story telling that binds
all of it together.
.
While narco culture is a reflection of the greater issues related to corruption
and violence in Mexico, it is still a powerhouse of representation of the
peoples who live this lifestyle.
.
Narco culture brings meaning to the life led by the participants in this
chapter of Mexican history.

.PP
.ls 2
If the narcocorrido does not encourage violence, and is not responsible for
being a gateway into the culture of the narco, then the question must by asked
why the Mexican government has been spending so much time, money, and effort in
attempting to address the genre and what it represents.
.
The Mexican government has attempted to control the spread of this music genre
through an array of censorship vehicles.
.[
state-censorship-and-controversy-narcocorrido
.]
The banning of the sale and distribution of narcocorridos as well as pressuring
radio stations to stop playing the music has been practiced by the State over
the years.
.
It is undeniable however that the narcocorrido has significant cultural
significance in Mexico and the genre reflects all of the social and economic
conditions facing the people of that nation at one point or another.
.[
state-censorship-and-controversy-narcocorrido
.]
It is apparent that studying the narcocorrido, an investigator is capable of
gaining insights and deep analytics about the beliefs, values, and attitude of
those in the drug trade.
.
You can manipulate your strategies to better combat drug traffickers by
understanding what they are thinking and how they feel.
.
Ultimately, knowledge of your enemy can lead to a tremendous reduction in
violence in the local community when law enforcement understands what that
community is experiencing.

.PP
.ls 2
Transnationalism, disparate cultural significance, authenticity and
masculinity, and an undercurrent of violence makes up the musical genre favored
by the narco.
.[
dark-side-latinidad
.]
Violent culture is a crucial element of self-expression in the producer and
consumer communities of the narcocorrido.
.[
dark-side-latinidad
.]
Violent culture asserts power and status where marginalisation exists.
.
Because the drug cartels use narcocorridos to promote their activities, recruit
new members, and to celebrate their power, it is important for an investigator
to grasp the complex link between cultural expression and power dynamics.
.
The investigator can develop a sensitivity to the range of information hidden
inside the narcocorrido and understand how social norms, actions, and even
broad socio-political context can affect what is happening in the drug world.

.PP
.ls 2
The narcocorrido perpetuates and reinforces the cultural values and norms of
narco terrorism by glorifying the violence, importance of loyalty, and
rejection of authority and law.
.
Mexican drug traffickers operate globally, but especially within the United
States.
.
This has led to the formation of a transnational identity among cultural
participants that is bound together by the narcocorrido.
.[
narco-transnational-identity
.]
This identity is also fragmented.
.
Different organizations in the narco community attempt to exert their power and
influence while supporting their favorite singers and sometimes killing the
signers of their rivals.
.
The narcocorrido is not just a reflection of the narco terrorism gripping
Mexico but actively plays a role in shaping the violence.
.[
narco-transnational-identity
.]
The narcocorrido is the story telling mechanism that assists in providing
normalization to the illicit activities and shapes the attitudes of those who
are listening to these songs.

.PP
.ls 2
A specific example of the use of the narcocorrido as a major vehicle for
positive proapaganda for the drug lords has been the work done by Joaquin "El
Chapo" Guzman.
.[
chapo-corrido-political-communication
.]
Guzman has been the central figure in numerous narcocorridos which have
glorified his life and criminal activities.
.
These songs have created a cult of personality around Guzman and solidified his
reputation as a masculine figure who is not hesitant to use violence in pursuit
of his goals.
.[
chapo-corrido-political-communication
.]
Guzman has deployed his wealth to fund political campaigns, artists, and others
who have each supported or been favorable to his interests.
.
The narcocorrido is an effective vehicle for exerting influence on others while
also building positive good will with individuals who may otherwise been
fearful of violence or less interested in supporting him.

.PP
.ls 2
The cultural expression of the narcocorrido trancends boundaries.
.
The new ability for people, ideas, and culture to flow across national borders
and influence people is undeniable.
.[
transnational-gaze-mexican-identity
.]
The narcocorrido reflects the capability for traditional Mexican music styles
to blend to the contemporary theme of drug trafficking.
.
The narcocorrido is a transnational identity for Mexican Americans by providing
a way for individuals to express a cultural identity and solidarity with the
concepts explored by the music.
.[
transnational-gaze-mexican-identity
.]
Mexican Americans are able to celebrate their culture through the story telling
and music of the narcocorrido.

.PP
.ls 2
The narcocorrido provides a voice to the historically maginalized speaker.
.
The music of the narcocorrido is criticized because it contains content that is
considered violent and misogynistic, but it also functions as a counterstory by
providing a perspective that uplifts minimized narratives about Mexican
culture.
.[
corridos-learning-development
.]
The narco is excluded from mainstream society and the subculture that
celebrates narco culture is a perspective that is rarely represented in
mainstream media.
.
The narcocorrido may be an alternative vehicle for explaining the complex
cultural and life experiences that the narco must navigate beyond a simple
viewpoint that trafficking must be eliminated.
.[
corridos-learning-development
.]
The narcocorrido may be a source for learning and development of alternative
views and that the stories provide an avenue for listeners to learn the
intertwined perspectives and experiences that the narco must navigate due to
his or her choice of lifestyle.

.bp

