.RP no
.P1
.ds DY
.so ./references/apa-ms.tmac
.R1
database ./references/references.ref
sort A+
accumulate
move-punctuation
label "(@|Q) ', ''\*[MIDINFO]' D '\*[ENDINFO]'"
short-label "'\*[MIDINFO]' D '\*[ENDINFO]'"
bracket-label " (" ")" "; "
join-authors " & " ", " " & "
no-label-in-reference
.R2

.TL
.hy 0
FROM NORMALIZATION TO CELEBRATION: Using social media and open source data
to track narco terrorism
.hy 1

.sp
.sp

.AU
Aaron L. Jones

.AI
Capitol Technology University

.AU
Dr. Michael Clayborn

.(C
2023-02-12
.)C

.bp

