.LP

.NH
.XN "Introduction"

.PP
Dating back to 1930, the earliest examples of corridos focused on narcotics
were sung by Juan Ramirez-Pimienta.
.
Typically the narcocorrido will speak of
real events, dates, and places and are positive in their opinion of the
activities of the drug trafickers are their actions.
.
The production of corridos focused on narcotics has become a thorn in officials
sides and government officials in Mexico have proposed drastic actions such as
banning narcocorridos.
.
The state of Chihuahua banned narcocorridos after passing a bill
introduced on April 12th by Congressman Ricardo Alan Boone.
.[
alb-jrnl-banned-in-chihuahua
.]
.
Premised on a belief that the music that glorifies and supports narco
terrorists benefits from being fact based, my research aims to answer the
following questions:
.
.sp
.nr step 1 1
.IP \n[step] 7
What is a narcocorrido?
.IP \n+[step]
What information can be obtained by the songs and their supporters.
.IP \n+[step]
Is this information useful to investigators?
.IP \n+[step]
Are there ethical issues with obtaining our data?
.IP \n+[step]
Are there technical issues with obtaining our data?
.IP \n+[step]
What can analysis of the data provide to an investigator?
.IP \n+[step]
What can we achieve by analyzing this data?

.bp

