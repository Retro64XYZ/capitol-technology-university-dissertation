.LP

.NH
.XN "Abstract"

.PP
Promoted and performed globally, narcocorridos are evocative and emotionally
driven ballads that soulfully describe and detail the exploits of Mexican
trafickers and assassins involved in narcotics.
.
The first popular narcocorridos landed in the 1970s but their explosive rise
into the mainstream started in the 1990s. Since the arrival of the
narcocorrido, it has been possible for anyone to hear about the ups and downs
about the narcotics trafficking business based on the songs that have provided
a detailed and sometimes deeply intimate view into the world of the drug
dealer, traficker, and narcoterrorist.
.
How does this music benefit researchers and investigators interested in combatting the
threat actors who are being praised and complimented by song?
.
By analyzing the comments and the music itself, investigators can gain a more
thorough and clear picture into the criminal underworld by tapping the social
queues, support, and knowledge base that the public has about the criminals and
their exploits. 

.bp
