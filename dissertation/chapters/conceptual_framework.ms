.LP

.NH
.XN "Conceptual Framework"

The identification with and consumption of narcocorridos could be a strong
indicator that the user is well versed in the drug trade. The comments and
sentiments of the consumers of narcocorridos could be of value to law
enforcement in the United States interested in combatting transnational
organized crime.

.bp

