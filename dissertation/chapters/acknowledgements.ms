.LP

.NH
.XN "Acknowledgements"

.PP
Reflecting deeply on my time spent at Capitol Technology University, it is
plain to any one who can see that none of my accomplishments would be possible
without the innumerable contributions made by my incredible mentors, supportive
friends, and patient family members.
.
It has not been lost on me that I could not have achieved my goal of receiving
my PhD without the support of the University Of Advancing Technology.
.
I am very luck that my employer chose to support me in my education journey and
provided me both the means as well as the encouragement to pursue my degree
plan.
.

.bp

